import React, {useState, useEffect, useRef} from "react";
import "./App.scss";
import ButtonAppBar from "./components/ButtonAppBar";
import MyContext from "./context";
import { Route, Switch, HashRouter, Redirect } from "react-router-dom";

import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import Welcome from "./views/Welcome";
import Capture from "./views/Capture";
import NotFound from "./views/NotFound";
import ClassRoom from "./views/classRoom";
import SignUp from "./views/SignUp";

import Admins from "./views/Admins";

import Schools from "./views/Schools";
import LinearProgress from "@material-ui/core/LinearProgress";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#33691e",
    },
    secondary: {
      //main: '#d4e157',
      main: "#3bcfa3", // https://www.sessions.edu/color-calculator/
    },
  },
});

function App () {
  //static contextType = MyContext;

  
  const [token,setToken] = useState('');
  const [id,setId] = useState('');
  const [authenticated, setAuthenticated] = useState('');
  const [loading, setLoading] = useState(true);
  const [location, setLocation] = useState('');
  const [isMount, setIsMount] = useState(true);

  useEffect(() => {
    if(isMount){
      setIsMount(false);
      setToken(localStorage.getItem("token") !== null ? localStorage.getItem("token") : '');
      setAuthenticated(localStorage.getItem("authenticated") !== null ? localStorage.getItem("authenticated") : '')

      return;
    } 
})

  /*componentWillMount() {
    this.setState({
      token:
        this.state.token === "" || null ? localStorage.getItem("token") : "",
      authenticated:
        this.state.authenticated === false
          ? localStorage.getItem("authenticated")
          : false,
    });
  }  */

  /*updateValue = (key, val) => {
    this.setState({ [key]: val });
  };*/

 /* componentDidUpdate() {
    localStorage.setItem("token", this.state.token);
    localStorage.setItem("authenticated", this.state.authenticated);
  }*/

    return (
      <MyContext.Provider
      value={{ token : [token, setToken],
         id: [id, setId],
         authenticated: [authenticated, setAuthenticated],
         loading: [loading, setLoading],
         location: [location, setLocation] }}
      >
        <MuiThemeProvider theme={theme}>
          <HashRouter>
            <Switch>
              <Route exact path="/welcome" component={Welcome} />
              <Route exact path="/sign-up" component={SignUp} />

              {  /*<Route exact path="/admins" component={Admins} />  */}

              <React.Fragment>
                <div className="App-Bar">
                  {
                    authenticated && <ButtonAppBar /> //&&  location.pathname   !== "/welcome"
                    //&& this.state.loading
                  }

                  {loading && (
                    <LinearProgress color="secondary" />
                  )}

                  <div
                    className={
                      "App-Content " + loading === true ? "no-clicks" : ""
                    }
                  >
                    <Switch>
                      <Route exact path="/classroom" component={ClassRoom} />
                     { /*<Route exact path="/capture" component={Capture} />
                      <Route exact path="/not-found" component={NotFound} />
                      <Route exact path="/schools" component={Schools} />   */
}
                      
                      <Redirect exact from="/" to="/welcome" />
                      <Redirect from="*" to="/not-found" />
                    </Switch>
                  </div>
                </div>
              </React.Fragment>
            </Switch>
          </HashRouter>
        </MuiThemeProvider>
      </MyContext.Provider>
    );
  
}
export default App;

import React, { useState, useEffect, useContext } from "react";

import TextField from "@material-ui/core/TextField";
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Grid,
  FormControl,
  MenuItem,
  Divider,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import "./index.scss";
import api from "../../libs/api";
import MyContext from "../../context";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";

import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import { useHistory } from "react-router-dom";

const ClassRoom = () => {

  const [name, setName] = useState('');
  const [type, setType] = useState('');

  const [classroom, setClassroom] = useState('');
  const [subject, setSubject] = useState('');
  const [daily, setDaily] = useState(0);
  const [extras, setExtras] = useState([{ name: '', value: 0 }]);

  const [totalExtras, setTotalExtras] = useState(0);
  const [percentageError, setPercentageError] = useState(false);



  const history = useHistory();

 const  registerGroup = (e:any) => {
    e.preventDefault();
    let totalExtra = 0;
    extras.filter((a) => {
      totalExtra = totalExtra + a.value;
    });
    setTotalExtras(totalExtra);

    if (daily + totalExtras != 100) {
      setPercentageError(true)
    }

    api
      .post("/gradeClassRooms", {
        classroom: classroom,
        subject: subject,
        administrator:
          "http://localhost:8080/api/administrators/" +
          localStorage.getItem("id"),
      })
      .then((response) => {
        console.log(response);

        api
          .post("/projectTypes", {
            name: "daily",
            percentage: daily,
            gradeClassRoom:
              "http://localhost:8080/api/gradeClassRoom/" + response.data.id,
          })
          .then((response) => {});

        extras.forEach((element) => {
          api
            .post("/projectTypes", {
              name: element.name,
              percentage: element.value,
              gradeClassRoom:
                "http://localhost:8080/api/gradeClassRoom/" + response.data.id,
            })
            .then((response) => {});
        });
      });
  };

  // handle click event of the Remove button
  const handleRemoveClick = (e:any, index:any) => {
    e.preventDefault();
    let list = [...extras];
    list.splice(index, 1);
    list.map((v, i) => {
      v.name = i+'';
    });
    setExtras( list )
    setPercentageError( false)
    //this.state.extras = list;
  };

  const handleAddClick = (e:any) => {
    e.preventDefault();

    setExtras([...extras, {name: '',value: 0}])
    setPercentageError(false);
      //percentageError: false,
  };

  const handleChangeEvaluations = (e:any, index:any) => {
    setPercentageError(false)

    extras.forEach((o, i, a) => {
      if (i == index) {
        a[index] = { name: index, value: +e.target.value };
      }
    });

    setExtras(extras)

  };

    return (
      <div className="sign-in-container">
        <Card className="card">
          <form className="form" onSubmit={registerGroup}>
            <CardHeader
              className="form-header"
              title="Classrooms"
              subheader="Register your classrooms"
            />
            <CardContent>
              <TextField
                className="form-control"
                label="Classroom name"
                fullWidth
                autoFocus
                required
                onChange={(event) => {
                  setClassroom(event.target.value)
                }}
              />

              <FormControl className="form-control">
                <InputLabel id="demo-simple-select-label">Subject</InputLabel>
                <TextField
                  label="demo-simple-select-label"
                  id="demo-simple-select"
                  value={subject}
                  onChange={(event) => {
                    setSubject(event.target.value)
                  }}                 
                />
              </FormControl>

              <Divider />

              <p>Evaluation weights</p>
              <div className="percentage">
                <TextField
                  id={"weeklyTotal"}
                  className="form-control"
                  label="Weekly Percentage"
                  fullWidth
                  autoFocus
                  value={daily}
                  onChange={(event) => {
                    setDaily(+event.target.value);
                  }}
                  error={percentageError}
                  helperText={
                    percentageError === true
                      ? "Sum not equal to 100"
                      : ""
                  }
                />
                <p>%</p>
              </div>

              {extras.map((x, i) => {
                return (
                  <div
                    key={x.name}
                    id={"Container " + x.name}
                    className="percentage"
                  >
                    <TextField
                      id={x.name + ""}
                      className="form-control weights"
                      label={"Extra " + x.name}
                      fullWidth
                      autoFocus
                      value={x.value}
                      error={percentageError}
                      helperText={
                        percentageError === true
                          ? "Sum not equal to 100"
                          : ""
                      }
                      onChange={(event) => {
                        handleChangeEvaluations(event, i);

                        // this.setState({ extras: event.target.value })
                      }}
                      // error={this.state.total !== 100}
                      //helperText={this.state.extras === "" ? 'Empty field!' : ' '}
                    />
                    <p>%</p>

                    <div className="btn-box">
                      {extras.length !== 1 && (
                        <RemoveIcon
                          className="mr10"
                          onClick={(e) => handleRemoveClick(e, i)}
                        >
                          Remove
                        </RemoveIcon>
                      )}
                      {extras.length - 1 === i && (
                        <AddIcon
                          onClick={(e) => {
                            handleAddClick(e);
                          }}
                        >
                          Add
                        </AddIcon>
                      )}
                    </div>
                  </div>
                );
              })}
            </CardContent>

            <CardActions
              className="form-buttons"
              style={{ justifyContent: "flex-end", alignItems: "" }}
            >
              <Button type="submit" color="primary">
                Register
              </Button>
            </CardActions>
          </form>
        </Card>
      </div>
    );
  
}

export default ClassRoom;

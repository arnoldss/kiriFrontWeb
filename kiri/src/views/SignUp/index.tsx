import React, { useState, useEffect, useContext } from "react";

import TextField from "@material-ui/core/TextField";
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Grid,
} from "@material-ui/core";
import "./index.scss";
import api from "../../libs/api";
import { states, types } from "./states";
import { makeStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";
//import Select, { SelectChangeEvent } from '@material-ui/core/Select';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { useHistory } from "react-router-dom";

const SignUp = () => {

  const [email, setEmail] = useState('');
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory();

  const signUp = (e: any) => {
    api
      .post("/createAdministrator", {
        username: username,
        email: email,
        password: password,
        school_ids: ''
      })
      .then((response) => {
        console.log(response);
      });


  };

  return (
    <div className="sign-in-container">
      {
        <Card>
          <form className="form" onSubmit={signUp}>
            <CardHeader
              title="Sign up"
              subheader="to register to Teacher helper"
            />
            <CardContent>
              <TextField
                label="Enter your email"
                fullWidth
                autoFocus
                required
                onChange={(event) => {
                  setEmail(event.target.value);
                }}
              />

              <TextField
                label="Enter your User Name"
                fullWidth
                required
                onChange={(event) => {
                  setUserName(event.target.value);
                }}
              />

              <TextField
                label="Enter your password"
                fullWidth
                required
                type="password"
                onChange={(event) => {
                  setPassword(event.target.value);
                }}
              />





            </CardContent>
            <CardActions style={{ justifyContent: "space-between" }}>
              <Button 
              onClick={() => {
                history.push("/welcome");
              }}
              >Cancel</Button>
            <Button type="submit" color="primary">
              Register
            </Button>
          </CardActions>
        </form>
      </Card>
      
}
</div >
    );
  
}

export default SignUp;

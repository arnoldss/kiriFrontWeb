import React, { Component, useContext } from 'react'
import PropTypes from 'prop-types'
import TextField from "@material-ui/core/TextField";
import { Card, CardHeader, CardContent, CardActions, Button, Grid, FormControl, MenuItem, Divider, FormControlLabel, Switch } from "@material-ui/core";
import './index.scss'
import api from "../../libs/api";
import MyContext from "../../context";
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import MaterialTable from 'material-table'



const { useState } = React;


class Capture extends React.Component {


  constructor() {
super();

this.state = {
  columns: [
    { title: 'Name', field: 'name' },
    { title: 'Surname', field: 'surname', initialEditValue: 'initial edit value' },
    { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
    {
      title: 'Birth Place',
      field: 'birthCity',
      lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
    },
  ],
  data: [
    { name: 'Mehmet', surname: 'Baran', birthYear: 1987, birthCity: 63 },
    { name: 'Zerya Betül', surname: 'Baran', birthYear: 2017, birthCity: 34 },
  ]

  
}

  }

  render() {

  


    return (



      

      <MaterialTable
      title="Editable Preview"
      columns={this.state.columns}
      data={this.state.data}
      editable={{
        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              this.setState({data : [...this.state.data, newData]});
              
              resolve();
            }, 1000)
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataUpdate = [...this.state.data];
              const index = oldData.tableData.id;
              dataUpdate[index] = newData;
              this.setState([...dataUpdate]);

              resolve();
            }, 1000)
          }),
        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataDelete = [...this.state.data];
              const index = oldData.tableData.id;
              dataDelete.splice(index, 1);
              this.setState({data : [...dataDelete]});
              
              resolve()
            }, 1000)
          }),
      }}
    />

    )
  }
}


export default Capture;
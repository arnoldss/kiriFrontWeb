export const states = [
  {code : 'AG' , label:  'AGUASCALIENTES'},
  {code :'BC' , label: 'BAJA CALIFORNIA'},
  {code :'BS' , label: 'BAJA CALIFORNIA SUR'},
  {code :'CH' , label: 'COAHUILA'},
  {code :'CI' , label: 'CHIHUAHUA'},
  {code :'CL' , label: 'COLIMA'},
  {code :'CP' , label: 'CAMPECHE'},
  {code :'CS' , label: 'CHIAPAS'},
  {code :'CDMX' , label: 'CIUDAD DE MÉXICO'},
  {code :'DG' , label: 'DURANGO'},
  {code :'GE' , label: 'GUERRERO'},
  {code :'GJ' , label: 'GUANAJUATO'},
  {code :'HD' , label: 'HIDALGO'},
  {code :'JA' , label: 'JALISCO'},
  {code :'MC' , label: 'MICHOACÁN'},
  {code :'MR' , label: 'MORELOS'},
  {code :'MX' , label: 'MÉXICO'},
  {code :'NA' , label: 'NAYARIT'},
  {code :'NL' , label: 'NUEVO LEÓN'},
  {code :'OA' , label: 'OAXACA'},
  {code :'PU' , label: 'PUEBLA'},
  {code :'QE' , label: 'QUERÉTARO'},
  {code :'QI' , label: 'QUINTANA ROO'},
  {code :'SI' , label: 'SINALOA'},
  {code :'SL' , label: 'SAN LUIS POTOSÍ'},
  {code :'SO' , label: 'SONORA'},
  {code :'TA' , label: 'TAMAULIPAS'},
  {code :'TB' , label: 'TABASCO'},
  {code :'TL' , label: 'TLAXCALA'},
  {code :'VC' , label: 'VERACRUZ'},
  {code :'YU' , label: 'YUCATÁN'},
  {code :'ZA' , label: 'ZACATECAS'}
  ];


  export const types = [
    "Preescolar", "Elementary", "High School"
  ]
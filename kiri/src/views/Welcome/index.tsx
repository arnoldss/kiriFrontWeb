import React, { useState, useEffect, useContext } from "react";
import TextField from "@material-ui/core/TextField";
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Grid,
} from "@material-ui/core";
import "./index.scss";
import SignUp from "../SignUp";
import api from "../../libs/api";
import MyContext from "../../context";
import { useHistory } from "react-router-dom";

const Welcome = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { authenticated, token, id } = useContext(MyContext);
  const [stateAuthenticated, setAuthenticated] = authenticated;
  const [stateToken, setToken] = token;
  const [stateId, setId] = id;
  const history = useHistory();

  const viewClassroom = () => {
    history.push("/classroom");
  }

  const signIn = (e: any) => {
    e.preventDefault();

    api
      .post("/authenticate", {
        username: email,
        password: password,
      })
      .then((response) => {
        console.log(response);
        setAuthenticated(true);
        setToken(response.data.token);
        setId(response.data.userId);

        localStorage.setItem("token", response.data.token);
        localStorage.setItem("id", response.data.userId);
        localStorage.setItem("school", response.data.role.id)
        viewClassroom();
      });
  };


  return (
    <div className="sign-in-container">
      <Card>
        <form className="form" onSubmit={signIn}>
          <CardHeader
            className="form-header"
            title="Sign in"
            subheader="to continue to Teacher helper"
          />
          <CardContent>
            <TextField
              label="Enter your email"
              fullWidth
              autoFocus
              required
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
            <TextField
              label="Enter your password"
              fullWidth
              required
              type="password"
              onChange={(event) => {
                setPassword(event.target.value);
              }}
            />
          </CardContent>
          <CardActions
            className="form-buttons"
            style={{ justifyContent: "space-between", alignItems: "" }}
          >
            <Button
              onClick={() => {
                history.push("/sign-up");
              }}
            >
              Register
            </Button>

            <Button type="submit" color="primary">
              Sign in
            </Button>
          </CardActions>
        </form>
      </Card>

    </div>
  );
}

export default Welcome;

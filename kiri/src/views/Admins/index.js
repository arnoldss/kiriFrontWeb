import React from "react";
import TextField from "@material-ui/core/TextField";
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Grid,
} from "@material-ui/core";
import "./index.scss";
import api from "../../libs/api";
import { makeStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";

class Admins extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signUp: true,
      email: "",
      username: "",
      password: "",
      schools: [],
      state: {},
      type: "",
    };
  }

  componentDidMount() {
    const { user, setUser } = this.context;
    console.log(user);
  }

  signUp = (e) => {
    e.preventDefault();
    api
      .post("/schools", {
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
      })
      .then((response) => {
        // this.setState({ users: response.data.data });

        console.log(response);
      });
  };

  showSignIn = () => {
    this.props.handleFormView(false);
  };


  loggedIn = false;
  render() {
    return (
      <Card>
        <form className="form" onSubmit={this.signUp}>
          <CardHeader
            title="Sign up"
            subheader="to register to Teacher helper"
          />
          <CardContent>
            <TextField
              label="Enter your email"
              fullWidth
              autoFocus
              required
              onChange={(event) => {
                this.setState({ email: event.target.value });
              }}
            />

           
          </CardContent>
          <CardActions style={{ justifyContent: "space-between" }}>
            <Button onClick={this.showSignIn}>Cancel</Button>
            <Button type="submit" color="primary">
              Register
            </Button>
          </CardActions>
        </form>
      </Card>
    );
  }
}

export default Admins;

import React, { useState, useEffect, Props } from 'react';
import clsx from 'clsx';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToApp from '@material-ui/icons/ExitToApp';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Exposure from '@material-ui/icons/Exposure';
import Print from '@material-ui/icons/Print';
import { withRouter } from "react-router-dom";
import { useHistory } from "react-router-dom";


const SwipeableTemporaryDrawer = (props: any) => {

  const {openLeftBar, toggleChildMenu, setTitle} = props?.props;

  
  const history = useHistory();

  const anchor = 'left';

  const toggleDrawer = (open: any) => (event: any) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    toggleChildMenu(open);

    //this.setState({ ...this.state, [anchor]: open });
    //this.props.updateBar(open);

  };

  const list = (anchor: any) => (
    <div
      role="presentation"
    /* { onClick={this.toggleDrawer(this.anchor, false)}
      onKeyDown={this.toggleDrawer(this.anchor, false)}}*/
    >
      <List>
        <ListItem button key={0} onClick={() => {
          history.push("/classroom");
          setTitle("ClassRooms")
        }}>
          <ListItemIcon><Edit> </Edit> </ListItemIcon>
          <ListItemText primary={"Classrooms"} />
        </ListItem>
        <ListItem button key={1} onClick={() => {
          history.push("/capture");
          setTitle("Capture Data")
        }}>
          <ListItemIcon><PhotoCamera> </PhotoCamera> </ListItemIcon>
          <ListItemText primary={"Grades - Assistance"} />

        </ListItem>


        <ListItem button key={2} onClick={() => {
          history.push("/averages");
          setTitle("Averages")
        }}>
          <ListItemIcon><Exposure> </Exposure> </ListItemIcon>
          <ListItemText primary={"Averages"} />
        </ListItem>
        <ListItem button key={3} onClick={() => {
          history.push("/delete");
          setTitle("Delete Data")
        }}>
          <ListItemIcon><Delete> </Delete> </ListItemIcon>
          <ListItemText primary={"Delete"} />
        </ListItem>
        <ListItem button key={4} onClick={() => {
          history.push("/sent-formats");
          setTitle("Sent Formats")
        }}>
          <ListItemIcon><Print> </Print> </ListItemIcon>
          <ListItemText primary={"Sent formats"} />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem button key={3}>
          <ListItemIcon>  <ExitToApp> </ExitToApp>  </ListItemIcon>
          <ListItemText primary={"Logout"} />
        </ListItem>
      </List>
    </div>
  );

  return (
    <React.Fragment>
      {
        <React.Fragment key={anchor}>
          <SwipeableDrawer
            anchor={anchor}
            open={openLeftBar}
            onClose={toggleDrawer(false)}
            onOpen={toggleDrawer(true)}
          >
            {list(anchor)}
          </SwipeableDrawer>
        </React.Fragment>
      }
    </React.Fragment>
  );

}


export default SwipeableTemporaryDrawer;
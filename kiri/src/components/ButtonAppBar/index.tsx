
import React, { useState, useEffect, useContext } from "react";
import './index.scss'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SwipeableTemporaryDrawer from "../SwipeableDrawer";
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import api from "../../libs/api";
import { Card, CardHeader, CardContent, CardActions, Button, Grid, FormControl, MenuItem, Divider, FormControlLabel, Switch } from "@material-ui/core";
import MyContext from '../../context';
import { useHistory } from "react-router-dom";

const ButtonAppBar = ()=> {

  //static contextType = MyContext;
  const history = useHistory();


  const [openLeftBar, setOpenLeftBar] = useState(true);
  const [title, setTitle] = useState('');



  const [classrooms, setClassrooms] = useState([]);
  const [actualClassroom, setActualClassroom] = useState({});
  
/*  componentDidMount() {
    //console.log(this.state);
    const { state, updateValue } = this.context;

    //console.log(state)
    api.get("/administrators/" + state.id + "/gradeClassRoomClassRoomSubjects"   //this actually works
    ).then(response => {
      //console.log("wey")
      console.log(response);
      let tempClassRooms = []
      let tempSubjects = [];
      let tempClassRoomsSubjects = [];

      response.data._embedded.gradeClassRooms.forEach((v) => {
        tempClassRoomsSubjects.push(v.grade + ' ' + v.classroom + '- '  + v.subject);

        tempClassRooms.push(v.grade + v.classroom);
        tempSubjects.push(v.subject)

      })
      this.setState({
        classRooms: tempClassRooms,
        subjects: tempSubjects,
        classRoomsSubjects: tempClassRoomsSubjects
      }, () => {
        updateValue("classRooms", this.state.classRooms);
        updateValue("classRoomsSubjects", this.state.classRoomsSubjects);

        updateValue("subjects", this.state.subjects);
        console.log(this.state);
      });
    });
  }  */


  /*componentDidUpdate() {
    
    const { state, updateValue } = this.context;
    console.log(state);

 //   this.state.classRooms =  state.classRooms;
 //   this.state.subjects = state.subjects;
 if(state.classRooms !== this.state.classRooms) {
    this.setState({
      classRooms: state.classRooms,
      subjects: state.subjects
    })
  }

  }*/



const toggleChildMenu = () => {
  setOpenLeftBar(!openLeftBar);
}

    return (
      <>
     <SwipeableTemporaryDrawer key={0}   props={{openLeftBar: openLeftBar, toggleChildMenu: toggleChildMenu, setTitle: setTitle}}   > </SwipeableTemporaryDrawer>

      <AppBar key={1} position="static" >
        <Toolbar>
          <IconButton edge="start" className="menuButton" color="inherit" aria-label="menu"
           onClick={toggleChildMenu}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className="title">
            {   title  }
          </Typography>

            {classrooms && classrooms.length && 

          <div className="flexContainer">
              <FormControl className="form-control">
                <InputLabel id="demo-simple-select-label">ClassRoom and Subject</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={actualClassroom}
                  onChange={(event: any) => { setActualClassroom(event.target.value ) }}
                >
                  {
                    classrooms && classrooms.map(el => <MenuItem value={el} key={el}> {el} </MenuItem>)
                  }
                </Select>
              </FormControl>


            </div>
  }

        </Toolbar>

      </AppBar>
</>
    ); 
}


export default ButtonAppBar;
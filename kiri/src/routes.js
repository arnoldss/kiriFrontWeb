import React from "react";
import { withRouter } from "react-router";
import {
	Switch,
	Route,
	Redirect
  } from "react-router-dom";

import Create from "./views/Create";
import NotFound from "./views/NotFound";
import ClassRoom from "./views/classRoom";
import SignUp from "./views/SignUp";

class Routes extends React.Component {
	render() {
		return(
			<Switch>
              <Route exact path="/classroom" component={ClassRoom} />
              <Route exact path="/create" component={Create} />
              <Route exact path="/create/:id" component={Create} />
              <Route exact path="/not-found" component={NotFound} />
              <Route exact path="/sign-up" component={SignUp} />

              <Redirect exact from="/" to="/welcome" />
              <Redirect from="*" to="/not-found" />
            </Switch>
		);
	}
}

export default withRouter(Routes);
